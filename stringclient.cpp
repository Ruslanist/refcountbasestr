#include "stringclient.h"
#include "smartpoint.h"
#include "refcountbase.h"
#include <cstring>

stringClient::stringClient(const char* strVal) : value( new stringStr(strVal) ) {

}

const char& stringClient::operator[](int it) const {

    return value->strData[it];
}

char& stringClient::operator[](int it) {

    if(value->lookShared() ) {

        value = new stringStr(value->strData);
    }

    value->setInFalsShared();
    return value->strData[it];
}

stringClient::stringStr::stringStr(const char* strVal) {

    logicRef(strVal);
}

stringClient::stringStr::stringStr(const stringStr& refObj) {

    logicRef(refObj.strData);
}

stringClient::stringStr::~stringStr() {

    delete [] strData;
}

void stringClient::stringStr::logicRef(const char *strVal) {

    strData = new char[strlen(strVal) + 1];
    strcpy(strData, strVal);
}
