#ifndef REFCOUNTBASE_H
#define REFCOUNTBASE_H

class refCountBase
{
protected:
    refCountBase();
    refCountBase(const refCountBase& refObj);
    refCountBase& operator=(const refCountBase& refObj);
    virtual ~refCountBase() =0;

public:    
    void addCountRef();
    void delCountRef();
    void setInFalsShared();
    bool lookShareable() const;
    bool lookShared() const;

private:
    int RefCountObj;
    bool shareableObj;
};

#endif // REFCOUNTBASE_H
