#include "refcountbase.h"

refCountBase::refCountBase() : RefCountObj(0), shareableObj(true) {

}

refCountBase::refCountBase(const refCountBase& refObj) : RefCountObj(0), shareableObj(true) {

}

refCountBase& refCountBase::operator=(const refCountBase &refObj) {

    return *this;
}

refCountBase::~refCountBase() {

}

void refCountBase::addCountRef() {

    ++RefCountObj;
}

void refCountBase::delCountRef() {

    if(--RefCountObj == 0) {

        delete  this;
    }
}

void refCountBase::setInFalsShared() {

    shareableObj = false;
}

bool refCountBase::lookShared() const {

    return  RefCountObj > 1;
}

bool refCountBase::lookShareable() const {

    return shareableObj;
}

