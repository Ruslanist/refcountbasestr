#ifndef SMARTPOINT_H
#define SMARTPOINT_H
#include "refcountbase.h"

template <typename TypClass>
class smartPoint
{
public:
    smartPoint(TypClass* ptrChild = nullptr);
    smartPoint(const smartPoint& refObj);
    ~smartPoint();
    smartPoint& operator=(const smartPoint& refObj);
    TypClass* operator->() const;
    TypClass& operator*() const;

private:
    TypClass* point;
    void logicRef();

};

#endif // SMARTPOINT_H
