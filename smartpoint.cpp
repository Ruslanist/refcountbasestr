#include "smartpoint.h"
#include "refcountbase.h"

template <typename TypClass>
smartPoint<TypClass>::smartPoint(TypClass* ptrChild) : point(ptrChild) {

    logicRef();
}

template <typename TypClass>
smartPoint<TypClass>::smartPoint(const smartPoint& refObj) : point(refObj.point) {

    logicRef();
}

template <typename TypClass>
smartPoint<TypClass>::~smartPoint() {

    if(point) {
        point->delCountRef();
    }
}

template <typename TypClass>
smartPoint<TypClass>& smartPoint<TypClass>::operator=(const smartPoint& refObj) {

    if(point != refObj.point) {

        TypClass* oldPoint = point;
        point = refObj.point;
        logicRef();
        if(oldPoint) {
            oldPoint->delCountRef();
        }
    }

    return *this;
}

template <typename TypClass>
void smartPoint<TypClass>::logicRef() {

    if(point == 0) {

        return ;
    }

    if(point->lookShareable() == false) {

        point = new TypClass(*point);
    }

    point->addCountRef();
}

template <typename TypClass>
TypClass* smartPoint<TypClass>::operator->() const {

    return point;
}

template <typename TypClass>
TypClass& smartPoint<TypClass>::operator*() const {

    return  *point;
}
