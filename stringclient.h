#ifndef STRINGCLIENT_H
#define STRINGCLIENT_H
#include "smartpoint.h"
#include "refcountbase.h"

class stringClient {

public:
    stringClient(const char *strVal = "");
    const char& operator[] (int it) const;
    char& operator[] (int it);

private:
    class stringStr : public refCountBase {
    public:
        stringStr(const char* strVal);
        stringStr(const stringStr& refObj);
        ~stringStr();
        char* strData;
        void logicRef(const char* strVal);
    };

    smartPoint<stringStr> value;

};

#endif // STRINGCLIENT_H
